# homework
# In[Given a 1D array, negate all elements which are between 3 and 8, in place (not created a new array).]
import numpy as np
x = np.random.randint(0, 10, 20)

result = x[(x<3) | (x>8)]

# In[Create random vector of size 10 and replace the maximum value by 0]

x = np.random.randn(10)

x[np.argmax(x)]=0

# In[How to find common values between two arrays?]
x = np.random.randint(0,100,20)
y = np.random.randint(0,100,10)

commonValue = np.intersect1d(x,y)

# In[Reverse a vector]
x = x[::-1]

# In[Create a 3x3 matrix with values ranging from 0 to 8]
x = np.random.randint(0,8,[3,3])

# In[Find indices of non-zero elements from the array [1,2,0,0,4,0]]
x = np.array([1,2,0,0,4,0])
x = x[x != 0]

# In[Create a 3x3x3 array with random values]
x = np.random.randint(0,10, [3,3,3])

# In[Create a random vector of size 30 and find the mean value]
x = np.random.randint(0,10,30)
mean_x = x.mean()

# In[Create a 2d array with 1 on the border and 0 inside]
x = np.zeros([2,10])
x[:,0]=1
x[:,-1]=1

# In[Given an array x of 20 integers in the range (0, 100)]
x = np.random.randint(0, 100, 20)
y = np.random.uniform(0, 20)

minDistance = (x-y)**2
index_min = np.argmin((x-y)**2)

if minDistance[index_min] == minDistance.min():
    print("right")
else:
    print("not")

