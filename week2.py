# -*- coding: utf-8 -*-
"""
Created on Sat Oct 13 21:08:04 2018

@author: nguye
"""
# In[1]:
# Write code to print out 1 to 100. If the number is a multiple of 3,
# print out "fizz" instead of the number. 
# If the number is a multiple of 5, print out "buzz". 
# If the number is multiple of 3 and 5, print out "fizzbuzz".

for i in range(1,100+1):
    if not(i%15): # because 3 and 5 are prime number
        print("fizzbuzz")
    elif not(i%3):
        print("fizz")
    elif not(i%5):
        print("buzz")
    else:
        print(i)
    
# In[2]:
# Let’s say I give you a list saved in a variable: a = [[1, 4], [9, 16], [25, 36]]. 
# Write one line of Python that takes this list a and makes a new list that contains [1, 4, 9, 16, 25, 36]

a = [[1, 4], [9, 16], [25, 36]]
# method 1:
b = [j for i in a for j in i]
# method 2:
#from itertools import chain
#b = list(chain.from_iterable(a))

# In[3]:
# Given a dictionary my_dict = {'a': 9, 'b': 1, 'c': 12, 'd': 7}. Write code to print out a list of sorted key based on their value. 
# For example, in this case, the code should print out ['b', 'd', 'a', 'c']

my_dict = {'a': 9, 'b': 1, 'c': 12, 'd': 7}
new_list = [key for key, value in sorted(list(my_dict.items()), key=lambda x : x[1])]
print(new_list)

# In[4]:
# Take two lists, say for example these two:

#	a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
    
#    b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
    
# and write a program that returns a list that contains only the elements that are common between the lists (without duplicates). 
# Make sure your program works on two lists of different sizes. 
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]

b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]

c =list(set(a).intersection(b))

# In[5]:
# Write a function that accept a string as a single argument and print out whether that string is a palindrome. (A palindrome is a string that reads the same forwards and backwards.) 
# For example, "abcddcba" is a palindrome, "1221" is also a palindrome.

def isPalindrome(array):
    if len(array)==1:
        return True
    for index_c in range(len(a)//2):
        if not(array[index_c] is array[-(index_c+1)]):
            return False
    return True

# In[6]:
#   You are given a string and width W. Write code to wrap the string into a paragraph of width W.
import textwrap
S = input()
W = int(input())
strWrap = textwrap.fill(S,W).split("\n")
strWrap
    